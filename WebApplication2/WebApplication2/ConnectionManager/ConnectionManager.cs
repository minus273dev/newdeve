﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace WebApplication2.ConnectionManager
{

    /* This class Represent Entier 
     * database connection in this project
     */ 

    public class DataConnectionManager 
    {

           
          public  MySqlConnection conection = new MySqlConnection();
          string ConnectionString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

            public MySqlConnection EstablishConection()
            
            {
                if (conection.State == ConnectionState.Broken || conection.State == ConnectionState.Closed)
                {
                    conection = new MySqlConnection(ConnectionString);
                  conection.Open();
                }
                return conection;
            }

            public void CloseConnection()
            {
                if (conection.State == ConnectionState.Open || conection.State == ConnectionState.Executing)
                {
                    conection.Close();
                }
            }
    }
}